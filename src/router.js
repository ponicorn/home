import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Whoami from './views/Whoami.vue'
import Contact from './views/Contact.vue'
import Projets from './views/Projets.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/whoami',
      name: 'whoami',
      component: Whoami
    },
    {
      path: '/contact',
      name: 'contact',
      component: Contact
    },
    {
      path: '/projets',
      name: 'projets',
      component: Projets
    }
  ]
})
