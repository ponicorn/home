import Vue from 'vue'
import VueI18n from 'vue-i18n'
import fr from './lang/fr.json'
Vue.use(VueI18n)

const messages = {
  fr
}

export default new VueI18n({
  locale: 'fr',
  messages
})
